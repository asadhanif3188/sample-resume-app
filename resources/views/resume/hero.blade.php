<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container" data-aos="fade-in">
      <h1>Asad Hanif</h1>
      <p>I'm <span class="typed" data-typed-items="Teacher, DevOps Engineer, Youtuber, Mentor"></span></p>
    </div>
  </section><!-- End Hero -->