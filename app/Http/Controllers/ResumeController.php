<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skill;

class ResumeController extends Controller
{
    function show()
    {
        $skills = Skill::all()->toArray();
        $skills_halved = array_chunk($skills, ceil(count($skills)/2));
        return view(
            'resume.index',
            [
                'skills_halved' => $skills_halved
            ]
        );
        
    }
}
