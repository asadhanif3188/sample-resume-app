<?php

use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Skill::create([
            'title' => 'Python',
            'percentage' => 90
        ]);

        App\Skill::create([
            'title' => 'C++',
            'percentage' => 95
        ]);

        App\Skill::create([
            'title' => 'C#',
            'percentage' => 80
        ]);

        App\Skill::create([
            'title' => 'Linux',
            'percentage' => 95
        ]);

        App\Skill::create([
            'title' => 'Laravel',
            'percentage' => 90
        ]);

        App\Skill::create([
            'title' => 'GitLab',
            'percentage' => 80
        ]);

        App\Skill::create([
            'title' => 'Docker',
            'percentage' => 90
        ]);

        App\Skill::create([
            'title' => 'Kubernetes',
            'percentage' => 90
        ]);

        App\Skill::create([
            'title' => 'MySQL',
            'percentage' => 80
        ]);

    }
}
